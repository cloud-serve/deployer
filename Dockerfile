FROM almir/webhook
MAINTAINER Marty Oehme <marty.oehme@gmail.com>

RUN apk add --no-cache git openssh

VOLUME repository

CMD ["-verbose", "-hooks=/webhooks.json"]
